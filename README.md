# Project: Template Files

This project contains multiple templates that can be used in sub-groups and projects.

```
.
├── .gitlab
│   ├── CODEOWNERS
│   ├── ci
│   ├── issue_templates
│   ├── merge_request_templates
│   └── route-map.yml
├── README.md
└── gitlab-ci
    ├── calc_gitlab-ci.yml
    └── calc_with_artifacts_gitlab-ci.yml
```